'use strict'

const gulp = require('gulp');
const babelify = require('babelify');
const browserify = require('browserify')
const sass = require('gulp-sass');
const pug = require('gulp-pug');
const source = require('vinyl-source-stream');
const buffer = require('vinyl-buffer');
const browserSync = require('browser-sync').create();
const sourcemaps = require('gulp-sourcemaps');
const uglify = require('gulp-uglify');
const data = require('gulp-data');


const packageJSON = require('./package.json');
const siteJSON = require('./src/data/site.json');
const contentJSON = require('./src/data/content.json');
const vendors = Object.keys(packageJSON.dependencies);

gulp.task('js-vendor', () => {
	console.log('bundling: ', vendors)
	return browserify()
    .require(vendors)
    .bundle()
    .pipe(source('vendor.bundle.js'))
    .pipe(gulp.dest('build/js'));
})

gulp.task('js-main', () => {
	browserify('src/js/main.js')
		.external(vendors)
		.transform('babelify', {
			presets: ['env', 'react'],
			plugins: ['transform-object-rest-spread']
		})
		.bundle()	
		.on('error', function(err){
			console.log(err.stack);
		})
		.pipe(source('main.js'))
		.pipe(buffer())
		.pipe(sourcemaps.init({loadMaps: true}))
		// .pipe(uglify())
		.pipe(gulp.dest('build/js'));
});

gulp.task('scss', () => {
	gulp.src('src/sass/main.scss')
		.pipe(sass().on('error', sass.logError))
		.pipe(gulp.dest('build/css'))
		.pipe(browserSync.stream());
})

gulp.task('images', () => {
	gulp.src('src/images/**/*')
		.pipe(gulp.dest('build/images'))
})

gulp.task('audio', () => {
	gulp.src('src/audio/**/*')
		.pipe(gulp.dest('build/audio'))
})


gulp.task('pug', function buildHTML() {
  return gulp.src('src/pug/*.pug')
  .pipe(pug({
		locals: {
			"site": siteJSON,
			"content": contentJSON
		},
		verbose: true
	}))
	.pipe(gulp.dest('build/'))
});

gulp.task('build', ['pug', 'images', 'js-vendor', 'js-main', 'scss', 'audio']);

gulp.task('default', ['js-main', 'images', 'js-vendor', 'pug'],() => {
	browserSync.init({
		server: "./build"
	});
	gulp.watch("src/sass/**/*", ['scss']);
	gulp.watch('src/**/*',['js-main', 'pug']).on('change', browserSync.reload)
});
