let AudioLibrary = {
  play: function(x,delay,volume) {
    let sample = new Audio(`/audio/${this[x].sound}`)
    sample.volume = volume || this[x].volume;
    
    setTimeout(() => {
      sample.play();
    }, delay || 0)
  },
  click: {
    sound: 'click.mp3',
    volume: .25
  },
  credit: {
    sound: 'credit.mp3',
    volume: .15
  }
}


export default AudioLibrary;