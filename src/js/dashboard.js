import AudioLibrary from './audio.js'

let dashboard = {

  betEl: document.querySelector('.dashboard__item--bet'),
  winEl: document.querySelector('.dashboard__item--win'),
  creditEl: document.querySelector('.dashboard__item--credits'),
  spinEl: document.querySelector('.spin'),
  
  betHandler(e, betEl, betBoxEl, spinEl) {

    let selectedBet = betEl.querySelector('#bet option:checked');
    if (e.target.dataset.increase) {

      if (selectedBet.nextElementSibling) {
        selectedBet.removeAttribute('selected');
        selectedBet.nextElementSibling.setAttribute('selected', true);
      }
    } else {
      if (selectedBet.previousElementSibling) {
        selectedBet.removeAttribute('selected');
        selectedBet.previousElementSibling.setAttribute('selected', true);
      }
    }

    betBoxEl.value = document.querySelector('#bet').value + 'x';
  },
  animatePayout(payout, credit) {
    let winBox = this.winEl.querySelector('.dashboard__statbox')
    let creditBox = this.creditEl.querySelector('.dashboard__statbox');
    winBox.value = payout;

    let newCredit = parseInt(credit - payout);

    setTimeout(() => {
      for (let index = 0; index < payout; index++) {
        setTimeout(() => {
          creditBox.value = parseInt(index + newCredit +1);
          AudioLibrary.play('credit');
        }, index * 90) 
      }
    }, 500)
  },
  init(spin) {

    this.betEl.addEventListener('click', (e) => {
      this.betHandler(e, 
        document.querySelector('.dashboard__item--bet'), 
        this.betEl.querySelector('.dashboard__statbox'), 
        document.querySelector('.spin'))
    });
    
    this.spinEl.addEventListener('click', () => {
      this.winEl.querySelector('.dashboard__statbox').innerHTML = '&nbsp;';
      spin(this.betEl.querySelector('#bet').value);
    })
  }
}

export default dashboard