import Reel from './reel.js';
import AudioLibrary from './audio.js'
import dashboard from './dashboard.js';

class Slots {
  constructor(el) {
    
    this.el = el;
    this.spinCount = 0;
    
    this.creditEl = document.querySelector('.dashboard__item--credits .dashboard__statbox');
    this.credit = 10;
    this.payTable = {
      1: 80, // 7
      2: 2, // cherry
      3: 25, // grape
      4: 10 // peach
    }

    this.init(el);
  }

  init(el) {
    // set reel heights
    let itemHeight = document.querySelector('.reel__item').offsetHeight;
    document.querySelectorAll('.reel__container').forEach(element => {
      element.style.height = itemHeight * 3 + 'px';
    });

    dashboard.init(this.spin.bind(this));
    this.creditEl.value = this.credit;
  }

  randomizer() {
    return Math.floor(Math.random() * (document.querySelectorAll(this.el + ' .reel__container').length)+1);
  }

  updateCredit(value, payout) {
    if (!payout) {
      this.credit = this.credit - value
      this.creditEl.textContent = this.credit;
    } else {
      this.credit = this.credit + value;
    }
  }

  spin(bet) {
    let playResults = [];
    let reels = [];

    this.updateCredit(bet);
  
    for (let i = 0; i < document.querySelectorAll(this.el + ' .reel__container').length; i++) {
      reels.push({
        el: document.querySelector('.reel__wrapper.reel-' + parseInt(i + 1)),
        obj: new Reel('.reel-' + parseInt(i+1))
      })
    }
    
    let x = 0;
    reels.forEach((reel,i) => {
      let val = this.randomizer();
      
      // receive promise from reel
      reel.spinState = reel.obj.spin(val);
      setTimeout(() => {
        reels[x].obj.spinning = false
        x++;
      }, Math.random() * (1000 - 500) + 1000 + (i *  Math.random() * 1500))  

      reel.spinState.then((response) => {
        playResults.push(val);
        

        // calculate payout on final reel
        if (i+1 === reels.length) {
          this.payout(playResults, bet)
        }
      })
    });
  }

  calculateReels(playResults, payTable) {

    // check if all 3 reels match
    if (playResults.every((x, i, arr) => {
        return x === playResults[0]
      })) {
      return payTable[playResults[parseInt(0)]];
    // count the cherries and return the calculated value
    } else if (playResults.includes(2)) {
      return payTable[2] * playResults.filter(val => val === 2).length;
    } else {
      return 0
    }
  }
  

  payout(playResults, bet) {
    let payout = this.calculateReels(playResults, this.payTable) * bet;

    // let winEl = document.querySelector('.dashboard__item--win .dashboard__statbox');

    // winEl.textContent = payout;
    this.updateCredit(payout, true);
    dashboard.animatePayout(payout, this.credit);

    
    
  }  
}

export default Slots;