import { TimelineMax } from "gsap";
import AudioLibrary from './audio.js';

class Reel {
  constructor(el) {
    this.tray = document.querySelector(el + '.reel__wrapper');
    this.spinning, this.reelItemHeight = 0;
    this.init(el);
  }

  init(el) {
    this.reelItemHeight = document.querySelector(el + ' .reel__item').offsetHeight;
  }

  spin(val) {
    this.reelSpin = new TimelineMax();
    this.spinning = true;
    // this.tray.style.height = parseInt(this.tray.firstChild.offsetHeight *2) + 'px';

    if (this.tray.children.length < 3) {
      this.tray.insertBefore(this.tray.lastChild.cloneNode(true), this.tray.firstChild);
      this.tray.insertBefore(this.tray.lastChild.cloneNode(true), this.tray.firstChild);
    }
    
    let reelStatus = new Promise((resolve, reject) => {

      this.reelSpin.add('start', 0)
      .set(this.tray, {y: 0})
      .to(this.tray, .25, {
        y: this.tray.firstChild.offsetHeight, 
        ease: Power0.easeNone
      })
      .set(this.tray, {y: 0})
      .add(() => {
        if (!this.spinning) {
          this.reelSpin.seek('seek');
        } else {
          this.reelSpin.seek('start');
        }
      })
      .add('seek')
      .to(this.tray, .5, {
        y: parseInt(this.reelItemHeight * val), 
        ease: Back.easeOut.config(1.7),
        onStart: () => {
          AudioLibrary.play('click')
          resolve()
        }
      })
    })
    return reelStatus;
  }
}

export default Reel;